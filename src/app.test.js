const chai = require('chai');
const chaiHttp = require('chai-http');
const url = 'https://drj335kkci.execute-api.sa-east-1.amazonaws.com/dev';
let payload = require("./users");
const Chance = require('chance');
const chance = new Chance();
let Email = chance.email({domain: "bossabox.com"})

chai.use(chaiHttp);
describe('User', () => {
  describe('/POST User', () => {
    it('Test email already registered', (done) => {
          chai.request(url)
          .post('/v1/users/')
          .send(payload) 
          .end((err, response) => {
              expect(response.body.error.message).toEqual("\"fullName\" é obrigatório");
              expect(response.body.error.code).toEqual('FULLNAME_REQUIRED');
              expect(response.statusCode).toBe(400);
              expect(response.headers.connection).toEqual("close");
              expect(response.headers['content-type']).toEqual('application/json; charset=utf-8');
              expect(response.headers['content-length']).toEqual('161');
              done();
            });
      });
    });

describe('/POST User', () => {
  it('Successful Registration', (done) => {
        let users =
          {
              "fullName": "Maria jose",
              "password": "123456",
              "email": Email,
              "loginType": "email"
          }
        chai.request(url)
        .post('/v1/users/')
        .send(users) 
        .end((err, response) => {
            expect(response.body.user.email).toBe(Email);
            expect(response.statusCode).toBe(200);
            expect(response.headers['content-type']).toEqual('application/json; charset=utf-8');
          done();
        });
  });
});

});
